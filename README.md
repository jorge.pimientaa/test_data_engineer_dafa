# test_data_engineer_dafa

## 1. Python and data engineering

### Answering the final questions of the instruction file
- Quels sont les éléments à considérer pour faire évoluer votre code afin qu’il puisse gérer de grosses
volumétries de données (fichiers de plusieurs To ou millions de fichiers par exemple) ?

    - Business needs.
    - The frequency of data refresh (daily, weekly, real time, etc).
    - Orchestration of pipeline jobs and steps.
    - Storage buckets and platform capabilities.
    - The kind of data to be stored and analysed.
    - On which systems is the date going to be stored and analysed.


- Pourriez-vous décrire les modifications qu’il faudrait apporter, s’il y en a, pour prendre en considération de
telles volumétries ?

    - The jobs must became smaller of more focusend on less quantity of actions.
    - Implement the usage of a distributed computation libraries, as PySpark for example.
    - Adapt the code in order to work with a data warehousing solution.
    - Greater modularisation of the code in order improve the adoption of newer use cases and maintenability.

### Posible improvements

- Replace the generic docker image (python:3.9.12-slim-buster) used for the Gitlab CI by a docker image specially built for the pipeline.
- Add more pipeline tests and unitary tests.
- Add more parameters to the TableConf, DataLoader, DrugsCitations and CitationsGraph to improve modularity and facilitate fine tuning and testing
- Add a deployment step toward a cloud data platform (GCP, AWS, etc...)

## 2. SQL
The two required SQL queries are to be found on the the folder sql:
- "part_1.sql"
- "part_2.sql"
