"""
Script to execute the load and transform and storage of the data
"""

import os

from conf.path_conf import PathConf
from load_transform.data_loader import DataLoader
from load_transform.data_storage import save_to_storage
from load_transform.tableconf import TableConf


def conf_files_list(conf_files_path):
    """
    Get a list of all the yaml table configuration files
    """
    files_list = [
        os.path.join(conf_files_path, x)
        for x in os.listdir(conf_files_path)
        if ".yaml" == os.path.splitext(x)[-1]
    ]

    return files_list


def get_data():
    """
    # Constructing a dictionary with all the loaded data
    """
    conf_list = conf_files_list(PathConf().tableconf_path)
    data = {}
    for conf_file in conf_list:
        table_conf = TableConf(conf_file)
        # In case a table is readed in multiple formats or in different parts
        if table_conf.table_name in data.keys():
            temp_loader = DataLoader(table_conf)
            temp_loader.append_data(data[table_conf.table_name], rename_cols=True)
            data.pop(table_conf.table_name)
            data[table_conf.table_name] = temp_loader
        else:
            data[table_conf.table_name] = DataLoader(table_conf)
    return data


if __name__ == "__main__":

    data = get_data()
    save_to_storage(data)
