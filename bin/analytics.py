"""
Script to execute analytics on the storage data
"""

from analytics_graph.citations_graph import CitationsGraph
from analytics_graph.drugs_citations import DrugsCitations
from analytics_graph.graph_export import save_to_storage

if __name__ == "__main__":
    citations = DrugsCitations()
    graph = CitationsGraph(citations)
    save_to_storage(graph.graph_dict)
