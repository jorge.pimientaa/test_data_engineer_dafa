"""
Load the data from input file
"""

import dateutil.parser
import numpy as np
import pandas as pd


class DataLoader:
    """
    Class containing the methods to read the each table from the input files
    """

    def __init__(self, tableconf) -> None:
        self.tableconf = tableconf
        self.raw_data = self.reading_data()
        self.clean_data = self.cleaning_data()

    def reading_data(self) -> pd.DataFrame:
        """
        Generic function for loading the data based on its tableconf format
        """
        if self.tableconf.loader_attr["format"] == "csv":
            data = pd.read_csv(
                self.tableconf.data_path,
                sep=self.tableconf.loader_attr["delimiter"],
                header=0,
                names=list(self.tableconf.table_columns.keys()),
                dtype=str,
                encoding="utf-8",
            )

        elif self.tableconf.loader_attr["format"] == "json":
            data = pd.read_json(
                self.tableconf.data_path,
                orient=self.tableconf.loader_attr["orient"],
                dtype=False,
                encoding="utf-8",
            )
        else:
            raise Exception("Loader format unrecognised or not specified")
        return data

    def cleaning_data(self) -> pd.DataFrame:
        """
        Modifying the format of each column and cleaning the whitespaces and NaNs from the data
        """
        df = self.raw_data.copy()

        for column, datatype in zip(
            self.tableconf.table_columns.keys(), self.tableconf.table_columns.values()
        ):
            if datatype == "date":
                df[column] = (
                    df[column]
                    .astype("str")
                    .apply(lambda x: dateutil.parser.parse(x))
                    .dt.date
                )

            elif datatype == "str":
                df[column] = df[column].astype(datatype)
                df[column] = self.clean_empty_strings(df[column])
                df[column] = df[column].astype(datatype).str.strip()
                df[column] = df[column].str.replace("\\\D\D\d|\\\D\d\d", "", regex=True)

            elif datatype == "int":
                df[column] = self.clean_empty_strings(df[column].copy())
                df[column] = pd.Series(
                    df[column].astype(float).to_list(), dtype="Int64"
                )

        return df

    def clean_empty_strings(self, column: pd.Series) -> pd.Series:
        # Replacing full whitespaces values by NaN
        whitespaces_index = column[
            column.str.replace(" ", "").str.len() == 0
        ].index.values
        if len(whitespaces_index) > 0:
            for index in whitespaces_index:
                column.iloc[index] = np.nan

        return column.astype(str)

    def append_data(self, data_loader, rename_cols=False):
        """
        Defining the behavior in case of table loaded on multiple parts or steps or
        in case of repeated table
        """
        # comparing the structure of the two tables
        raw_test = self.raw_data.equals(data_loader.raw_data)
        clean_test = self.clean_data.equals(data_loader.clean_data)

        if raw_test and clean_test:
            self.tableconf = [self.tableconf, data_loader.tableconf]
            self.raw_data = [self.raw_data, data_loader.raw_data]
            self.clean_data = [self.clean_data, data_loader.clean_data]
        else:
            self.tableconf = [self.tableconf, data_loader.tableconf]
            if rename_cols:
                raw_cols = dict(
                    zip(
                        data_loader.raw_data.columns.to_list(),
                        self.raw_data.columns.to_list(),
                    )
                )
                data_loader.raw_data = data_loader.raw_data.rename(columns=raw_cols)
                clean_cols = dict(
                    zip(
                        data_loader.clean_data.columns.to_list(),
                        self.clean_data.columns.to_list(),
                    )
                )
                data_loader.clean_data = data_loader.clean_data.rename(
                    columns=clean_cols
                )

            self.raw_data = pd.concat([self.raw_data, data_loader.raw_data], axis=0)
            self.clean_data = pd.concat(
                [self.clean_data, data_loader.clean_data], axis=0
            )

        # self.cleaning_data()
