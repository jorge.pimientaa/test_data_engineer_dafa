"""
Read, parse and store the tableconf properties for one input file
"""
import os

import yaml

from conf.path_conf import PathConf


class TableConf:
    """
    Class to parse and hold the TableConf data needed to load the files
    """

    def __init__(self, conf_path,data_path=None) -> None:
        self.parse_conf(conf_path)
        self.name()
        self.loader()
        self.columns()
        if data_path is None:
            self.data_path = os.path.join( PathConf().raw_data_path, self.loader_attr["wildcard"] )
        else:
            self.data_path = data_path

    def parse_conf(self, conf_path):
        """
        Reading and parsing the table conf yaml file
        """
        self.conf_path = conf_path
        with open(self.conf_path, "r", encoding="utf-8") as file:
            try:
                self.conf = yaml.safe_load(file)
            except yaml.YAMLError as load_error:
                raise Exception(
                    f"Conf file << {self.conf_path} >> is not found or not readable"
                ) from load_error

    def name(self):
        """
        Retriving the name of the table to be created
        """
        self.table_name = self.conf["table"]

    def loader(self):
        """
        Retriving the loader parameters to using in the load of the data
        """
        self.loader_attr = self.conf["loader"]

    def columns(self):
        """
        Retriving name of the columns and its data types
        """
        columns_dict = {}
        for column in self.conf["columns"]:
            columns_dict[column["name"]] = column["type"]
        self.table_columns = columns_dict
