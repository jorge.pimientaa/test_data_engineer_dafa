"""
Functions need to store the readed and cleaned data
"""

import os

from conf.path_conf import PathConf


def save_to_storage(data):
    """
    Function to save the loaded files to a cleanen and well formated csv
    """
    if not os.path.exists(PathConf.data_storage_path):
        os.mkdir(PathConf.data_storage_path)

    for table, loader in zip(data.keys(), data.values()):
        save_path = os.path.join(PathConf.data_storage_path, table + ".csv")
        loader.clean_data.to_csv(save_path, sep="," , index=False)
