"""
Adhoc extraction from the drugs citation graph
"""
import json
import os

import pandas as pd

from conf.path_conf import PathConf


def read_citations_graph():
    """
    Read the json citation graph into a python dictionary
    """
    json_file_path = os.path.join(
        PathConf.analytics_storage_path, "drug_citations_graph.json"
    )
    with open(json_file_path, "r", encoding="utf-8") as json_file:
        return json.load(json_file)


def journal_citations_agg(graph):
    """
    Aggregate the readed data in order to know which journal cited thes most drugs
    """
    df_list = []
    for drug in graph.keys():
        temp_df = pd.DataFrame()
        temp_df["journal"] = graph[drug]["journal"]
        temp_df["drug"] = drug
        df_list.append(temp_df)

    df_journals = pd.concat(df_list, axis=0, ignore_index=True)
    df_agg = df_journals.groupby("journal").count()

    return df_agg


if __name__ == "__main__":
    graph_dict = read_citations_graph()
    citations_agg = journal_citations_agg(graph_dict)
