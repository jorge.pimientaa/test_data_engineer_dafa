"""
    Path parameters setup
"""
from dataclasses import dataclass


@dataclass
class PathConf:
    """
    Class to contain the path parameters need for the application to run
    """

    tableconf_path = r"./conf/tableconf"
    raw_data_path = r"./raw_data"
    data_storage_path = r"./data_storage"
    analytics_storage_path = r"./analytics_storage"
    drugs_analytics_tables = {
        "clinical_trials": "clinical_trials.csv",
        "drugs":"drugs.csv",
        "pubmed":"pubmed.csv",
    }
