"""
Creation of the json graph file from the graph dictionary
"""

import json
import os

from conf.path_conf import PathConf


def save_to_storage(graph_dict):
    """
    Dump the graph dictionary into json format
    """
    if not os.path.exists(PathConf.analytics_storage_path):
        os.mkdir(PathConf.analytics_storage_path)

    save_path = os.path.join(
        PathConf.analytics_storage_path, "drug_citations_graph.json"
    )
    with open(save_path, "w", encoding="utf-8") as json_file:
        json.dump(graph_dict, json_file)
