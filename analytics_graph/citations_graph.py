"""
Collecting the drugs citations on journals
"""


class CitationsGraph:
    """
    Creation of the citations graphs from the already created drugs citations data
    """

    def __init__(self, citations) -> None:
        self.citations = citations.drug_citations
        self.graph_dict = self.create_graph_dict()

    def create_graph_dict(self):
        """
        Creation of the citations graph dictionary
        """
        graph_dict = {}
        for drug in self.citations.keys():
            graph_dict[drug] = {
                "journal": self.citations[drug]["journal"].to_list(),
                "date": self.citations[drug]["date"].to_list(),
                "scientific_title": self.citations[drug]["scientific_title"].to_list(),
            }
        return graph_dict
