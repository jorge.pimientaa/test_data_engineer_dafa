"""
Collecting the drugs citations on journals
"""
import os
import pandas as pd

from conf.path_conf import PathConf


class DrugsCitations():
    """
    DrugsCitations class creating and storing the necessary information for
    later construction of the citations graph
    """

    def __init__(self,data_storage=None) -> None:
        if data_storage is None:
            self.data_storage = PathConf().data_storage_path
        else:
            self.data_storage = data_storage
        self.read_data()
        self.get_drug_citations()

    def read_data(self):
        """
        Reading the data from all the stored tables
        """
        data = {}
        for table, file_name in zip(
            PathConf().drugs_analytics_tables.keys(),
            PathConf().drugs_analytics_tables.values(),
        ):
            file_path = os.path.join(self.data_storage,file_name)
            data[table] = pd.read_csv(file_path, sep=",")
        self.tables = data

    def get_drug_citations(self):
        """
        Querying the loaded tables to collect and organise the needed data
        for the creation of the citations graph
        """
        drugs = self.tables["drugs"].copy()
        pubmeds = self.tables["pubmed"].copy()
        clinical_trials = self.tables["clinical_trials"].copy()

        drug_citations_dict = {}
        for drug in drugs["drug"]:
            temp_pubmeds = pubmeds.query(
                "scientific_title.str.contains(@drug,case=False,na=False)"
            )
            ##temp_pubmeds["type"] = "pubmeds"
            temp_trials = clinical_trials.query(
                "scientific_title.str.contains(@drug,case=False,na=False)"
            )
            ##temp_trials["type"] = "trials"

            drug_citations_dict[drug] = pd.concat([temp_pubmeds, temp_trials], axis=0)

        self.drug_citations = drug_citations_dict
