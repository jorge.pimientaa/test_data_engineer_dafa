SELECT * FROM (
    SELECT
        transactions.client_id AS client_id,
        products.product_type AS product_type,
        SUM(prod_price) * SUM(prod_qty) AS ventes
    FROM TRANSACTION AS transactions
    JOIN PRODUCT_NOMENCLATURE AS products
        ON products.product_id = transactions.prop_id
    GROUP BY
        client_id,
        product_type
    )
PIVOT(
    SUM(ventes) AS ventes FOR product_type IN ('MEUBLE','DECO')
    )
