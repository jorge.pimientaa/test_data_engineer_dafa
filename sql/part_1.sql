SELECT
    date,
    SUM(prod_price) * SUM(prod_qty) AS ventes
FROM TRANSACTION
WHERE
    date BETWEEN "2019-01-01" AND "2019-12-31"
GROUP BY
    date
ORDER BY 
    date