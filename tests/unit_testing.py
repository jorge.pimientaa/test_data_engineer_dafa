import pytest
import json
import pandas as pd
from load_transform.tableconf import TableConf
from load_transform.data_loader import DataLoader
from analytics_graph.drugs_citations import DrugsCitations
from analytics_graph.citations_graph import CitationsGraph



def test_table_conf():
    table_conf1 = TableConf(r'./tests/data/yaml/drugs.yaml',data_path=r"./tests/data/tables/drugs_raw.csv")
    table_conf2 = TableConf(r'./tests/data/yaml/pubmed2.yaml')
    assert table_conf1.table_name == "drugs"
    assert table_conf1.loader_attr['delimiter'] == ","
    assert table_conf2.loader_attr["delimiter"] == "None"
    assert table_conf2.loader_attr['format'] == "json"

def test_dataloader():
    table_conf = TableConf(r'./tests/data/yaml/drugs.yaml',data_path=r"./tests/data/tables/drugs_raw.csv")
    loader = DataLoader(tableconf=table_conf)
    df_pass = pd.read_csv(r"./tests/data/tables/drugs_pass.csv",sep=",")
    pd.testing.assert_frame_equal(df_pass,loader.clean_data)

def test_citations_graph():
    citations = DrugsCitations(data_storage=r"./tests/data/data_storage_test")
    result_graph = CitationsGraph(citations)
    with open("./tests/data/json/drug_citations_graph.json", "r", encoding="utf-8") as json_file:
        expected_graph = json.load(json_file)
    assert result_graph.graph_dict==expected_graph
